from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Pattern(models.Model):
    name = models.CharField(max_length=20)
    background = models.ImageField(blank=True)
    orient = models.BooleanField(default=True)
    json_fields = models.TextField(blank=True)
    def __str__(self):
        return self.name

class Certificate(models.Model):
    pattern_name = models.CharField(max_length=20)
    certificate_name = models.CharField(max_length=20)
    json_fields = models.TextField()
