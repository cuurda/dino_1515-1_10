from django.contrib import admin
from .models import Certificate, Pattern

# Register your models here.

admin.site.register(Certificate)
admin.site.register(Pattern)