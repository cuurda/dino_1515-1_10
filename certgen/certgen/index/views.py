import json
from django.shortcuts import render
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from .models import Pattern

# Create your views here.
def add_pattern(request):
    if request.method == 'GET':
        response = ""
        try:
            deJson = json.loads(request.GET["json"])
            patt = Pattern(name=deJson["name"], json_fields = deJson["json_map"], orient= deJson["orient"])
            patt.save()
            response = HttpResponse(patt.id)
        except MultiValueDictKeyError:
            response = HttpResponse("Uncorrect JSON")
        return response

def get_pattern(request):
    if request.method == 'GET':
        response = ""
        pattern_id = request.GET["pattern_id"]
        patterns = Pattern.objects.filter(id=pattern_id)
        response = HttpResponse(patterns)
        return response