string = input().split()
n = int(string[0])
m = int(string[1])
k = int(string[2])

current_matrix = []
for i in range(n):
    current_matrix.append(input().split())

print(current_matrix)
next_matrix = current_matrix[:]

move_matrix = []
for i in range(n):
    move_matrix.append([0 for j in range(m)])

for i in range(k):
    for y in range(n):
        for x in range(m):
            leftme = x - 1
            rightme = x + 1
            if leftme < 0:
                leftme = None
            if rightme >= m:
                rightme = None
            upme = y - 1
            downme = y + 1
            if upme < 0:
                upme = None
            if downme >= n:
                downme = None
            stable = 0
            active = 0
            for a in [(leftme,y), (rightme,y), (x, upme), (x, downme)]:
                if not (a[0] is None or a[1] is None):
                    v = a[0]
                    u = a[1]
                    print("X:", v, "Y: ", u)
                    if current_matrix[u][v] == '2':
                        stable+=1
                        active+=1
                    elif current_matrix[u][v] == '3':
                        active+=1
            print(stable,active)
            if stable > 1:
                next_matrix[x][y] = '2'
            elif active > 0:
                next_matrix[x][y] = '3'
            else:
                next_matrix[x][y] = '1'
            if not current_matrix[x][y] == next_matrix[x][y]:
                move_matrix[x][y] += 1
    current_matrix = next_matrix[:]
    print(current_matrix)

print(move_matrix)