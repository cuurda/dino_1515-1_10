string = input().split()
n = int(string[0])
m = int(string[1])

block_lists = [0 for i in range(n)]

for i in range(m):
    string = input().split()
    s = int(string[0])
    l = int(string[1])-1
    r = int(string[2])-1
    d = int(string[3])
    accepted = True
    for j in range(l,r+1):
        if block_lists[j] >= s:
            accepted = False
            break
    if accepted:
        print("Yes")
        for j in range(l,r+1):
            block_lists[j] = s+d
    else:
        print("No")