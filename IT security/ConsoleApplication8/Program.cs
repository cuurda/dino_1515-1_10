﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System;
using System.Numerics;

namespace ConsoleApplication8
{
    class Program
    {
        private struct ExtendedEuclideanResult {
            public int x;
            public int y;
            public int gcd;
        }

        private static ExtendedEuclideanResult ExtendedEuclide(int a, int b)
        {
            int x = 1;
            int d = a;
            int v1 = 0;
            int v3 = b;
            while (v3 > 0)
            {
                int q0 = d / v3;
                int q1 = d % v3;
                int tmp = v1 * q0;
                int tn = x - tmp;
                x = v1;
                v1 = tn;
                d = v3;
                v3 = q1;
            }
            int tmp2 = x * (a); tmp2 = d - (tmp2);
            int res = tmp2 / (b);
            ExtendedEuclideanResult result = new ExtendedEuclideanResult()
            {
                x = x,
                y = res,
                gcd = d
            };
            return result;
        }
        static void Resheto(uint start, uint end, uint seed, ref List<uint> list )
        {
            bool div;
            for (uint i = start + 1; i < end; i++)
            {
                
                div = false;
                for (uint j = 3; j <= (uint)Math.Sqrt(30 * i + seed) + 1; j += 2)
                {
                    if ((30 * i + seed) % j == 0)
                    {
                        div = true;
                        break;
                    }
                }
                if (!div)
                {
                    list.Add(30 * i + seed);
                }
            }

        }
        static async void das(uint start, uint end, List<uint> keys)
        {
            Task<string>.Factory.StartNew(() =>
            {
                Resheto(start, end, 1, ref keys);
                return "ok";
            });
            Task<string>.Factory.StartNew(() =>
            {
                Resheto(start, end, 7, ref keys);
                return "ok";
            });
            Task<string>.Factory.StartNew(() =>
            {
                Resheto(start, end, 11, ref keys);
                return "ok";
            });
            Task<string>.Factory.StartNew(() =>
            {
                Resheto(start, end, 13, ref keys);
                return "ok";
            });
            Task<string>.Factory.StartNew(() =>
            {
                Resheto(start, end, 17, ref keys);
                return "ok";
            });
            Task<string>.Factory.StartNew(() =>
            {
                Resheto(start, end, 19, ref keys);
                return "ok";
            });
            Task<string>.Factory.StartNew(() =>
            {
                Resheto(start, end, 23, ref keys);
                return "ok";
            });
            Task<string>.Factory.StartNew(() =>
            {
                Resheto(start, end, 29, ref keys);
                return "ok";
            });
        }
        static void Main(string[] args)
        {
            bool div;
            uint[] exponents = new uint[]{ 3U, 5U, 17U, 257U };
            uint start = 512 / 30;//768 / 30; 
            uint end = 1024 / 30;//5535 / 30;
            List<uint> keys = new List<uint>();
            das(start, end, keys);
            Thread.Sleep(100);
            Random rnd = new Random();
            uint p = keys[rnd.Next(keys.Count - 1)];
            uint q = keys[rnd.Next(keys.Count - 1)];
            uint n = p * q;
            uint phi = (p-1) * (q - 1);
            ExtendedEuclideanResult node = ExtendedEuclide((int)phi, (int)exponents[3]);
            int d = node.y;
            Console.WriteLine(p);
            Console.WriteLine(q);
            Console.WriteLine(n);
            Console.WriteLine(phi);
            Console.WriteLine(exponents[3]);
            Console.WriteLine(d);
            Console.WriteLine(node.x);
            Console.WriteLine(node.y);
            Console.WriteLine(node.gcd);
            Console.WriteLine("Keys are initilized.");
            Console.Write("Input word: ");
            char[] word = Console.ReadLine().ToCharArray();
            BigInteger[] crypt = new BigInteger[word.Length];
          /*for (int i = 0; i < word.Length; i++)
            {
                Console.WriteLine((int)word[i]);
            }*/
            for (int i = 0; i < word.Length; i++)
            {
                crypt[i] = BigInteger.ModPow(new BigInteger((int)word[i]), new BigInteger(exponents[3]), new BigInteger(n));
            }
            string encoded = ""; 
            for (int i = 0; i < word.Length; i++)
            {
                encoded += crypt[i].ToString("X");
            }
            Console.WriteLine(encoded);
            Console.WriteLine("__________________________________");
            BigInteger[] decrypt = new BigInteger[crypt.Length];
            string decoded = "";
            for (int i = 0; i < crypt.Length; i++)
            {
                decrypt[i] = BigInteger.ModPow(crypt[i], new BigInteger(d), new BigInteger(n));
                decoded += (char)decrypt[i];
            }
            Console.WriteLine(decoded);
            Console.Read();
            
        }


    }
}













