square = [ 
	[],[],[],[],
	[],[],[],[],
	[],[],[],[],
	[],[],[],[],
	[],[],[],[],
	[],[],[],[],
	[],[],
]

def init():
	begin_char = 97
	for j in range(0,26):
		char = 97 + j  
		for i in range(0,26):
			if char > 122:
				char = 97 
			square[j].append(chr(char))
			char = char + 1; 

def encrypt(msg, key):
	n = len(msg)
	m = len(key)
	res = "";
	for i in range(0,n):
		res += square[ord(key[i%m])-97][ord(msg[i])-97]		
	return res;	  	
	
def decrypt(msg, key):
	n = len(msg)
	m = len(key)
	res = "";
	for i in range(0,n):
		res += square[0][square[ord(key[i%m])-97].index(msg[i])]
	return res;	  
			
init()		
while True:
	print("Please, input E for encrypt or D for decrypt")
	ans = str(input())
	if ans=="E":
		print("Encrypt mode")
		print("Input string:")
		string = str(input())
		print("Input key:")	
		key = str(input())
		print(encrypt(string,key)) 
	if ans=="D":
		print("Decrypt mode")
		print("Input string:")
		string = str(input())
		print("Input key:")	
		key = str(input())
		print(decrypt(string,key))
