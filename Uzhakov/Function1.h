namespace CuurdaIntegrator{
	class Function1 { //f(x)=(sin(ax))^n*x^(m/k)
		private:
			float a, n, m, k;
		public:
			Function1();
            Function1(float in_a, float in_n, float in_m, float in_k);
			float calculate(float x);
			//float operator()(float x);
	};
}

