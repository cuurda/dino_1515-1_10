#include <string>
#include <cmath>

#include "Function1d.h"
namespace CuurdaIntegrator{
	Function1::Function1(){
			bool checked = false;
			while(!checked){
				cout<<"Введите a:\n";
				checked = read_number(a, -100.0, 100.0);
			}
			checked = false;
			while(!checked){
				cout<<"Введите n:\n";
				checked = read_number(n, -100.0, 100.0);
                	}
			checked = false;
			while(!checked){
				cout<<"Введите m:\n";
				checked = read_number(m, -100.0, 100.0);
			}
			checked = false;
			while(!checked){
                                cout<<"Введите k:\n";
                                checked = read_number(k, -100.0, 100.0);
                        }
		};
    Function1::Function1(double in_a, double in_n, double in_m, double in_k){
        a = in_a;
        n = in_n;
        m = in_m;
        k = in_k;
    };
	double Function1::calculate(double x){
		return pow(sin(a*x),n)*pow(x,m/k);
	};
/*        double Function1::operator()(double x){
                return pow(sin(a*x),n)*pow(x,m/k);
        };*/
}

