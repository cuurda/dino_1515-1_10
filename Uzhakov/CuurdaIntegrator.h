#include <iostream>
using namespace std;
namespace CuurdaIntegrator {
	template< class Class >
        float calculate_integral(Class* Function, float down, float up, float delta);

        void print_modes();
        void select_function();
	void print_hello();
	bool read_number(float &num, float min, float max);
    bool read_number(double &num, double min, double max);
    bool read_number(long double &num, long double min, long double max);
	void read_limits(float* limits);

    template <typename Number>
    class Function1;
	class Function2;
}
