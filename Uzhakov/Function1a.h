namespace CuurdaIntegrator{
    template <typename Number>
	class Function1 { //f(x)=(sin(ax))^n*x^(m/k)
		private:
			Number a, n, m, k;
		public:
			Function1();
            static Number count(Number in_a, Number in_n, Number in_m, Number in_k, Number in_x);
			Number calculate(Number x);
			Number operator()(Number x);
	};
}

