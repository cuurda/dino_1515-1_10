#include <string>
#include <cmath>

#include "Function1a.h"
namespace CuurdaIntegrator{
    
    template <typename Number>
	Function1<Number>::Function1(){
        static_assert ( is_same<Number, float>::value || is_same<Number, double>::value || is_same<Number, long double>::value, "Sorry, inconsictent type!" );
			bool checked = false;
			while(!checked){
				cout<<"Введите a:\n";
				checked = read_number(a, -100.0, 100.0);
			}
			checked = false;
			while(!checked){
				cout<<"Введите n:\n";
				checked = read_number(n, -100.0, 100.0);
                	}
			checked = false;
			while(!checked){
				cout<<"Введите m:\n";
				checked = read_number(m, -100.0, 100.0);
			}
			checked = false;
			while(!checked){
                                cout<<"Введите k:\n";
                                checked = read_number(k, -100.0, 100.0);
                        }
		};
    template <typename Number>
    Number Function1<Number>::count(Number in_a, Number in_n, Number in_m, Number in_k, Number x){
        static_assert ( is_same<Number, float>::value || is_same<Number, double>::value || is_same<Number, long double>::value, "Sorry, inconsictent type!" );
        return pow(sin(in_a*x),in_n)*pow(x,in_m/in_k);
    };
    template <typename Number>
	Number Function1<Number>::calculate(Number x){
		return pow(sin(a*x),n)*pow(x,m/k);
	};
    template <typename Number>
    Number Function1<Number>::operator()(Number x){
        return pow(sin(a*x),n)*pow(x,m/k);
    };
}

