namespace CuurdaIntegrator{
	class Function1 { //f(x)=(sin(ax))^n*x^(m/k)
		private:
			double a, n, m, k;
		public:
			Function1();
            Function1(double in_a, double in_n, double in_m, double in_k);
			double calculate(double x);
			//double operator()(double x);
	};
}

