#include "Checker.h"
 
read_number(float &num, float  min, float max){
        string snum;
        cin>>snum;
        num = stof(snum);
        bool right = num <= max && num >= min;
        if (!right){
                cout<<"\033[1;31mНедопустимое значение параметра!\033[0m\n";
        }
	return right;
}

