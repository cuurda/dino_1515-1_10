#include "CuurdaIntegrator.h"
#include "Function1a.cpp"
#include "Function2.cpp"

namespace CuurdaIntegrator{
	void print_hello(){
        	cout<<"Вас приветствует программа \"Расчет интегралов\"\n";
	        print_modes();
	}
	void print_modes(){
        	cout<<"Выберите функцию, чтобы посчитать определенный интеграл:\n";
	        cout<<"\t1) f(x)=(sin(ax))^n*x^(m/k)\n";
        	cout<<"\t2) f(x)=d*e^(-g*x^n)*x^(m/k)\n";
        	select_function();
	}

	void select_function(){
        	string mode;
	        cin>>mode;
        	if (mode == "1"){
                	Function1<float>* function = new Function1<float>;
			float* limits = new float[2]; 
			read_limits(limits);
			float result = calculate_integral(function, limits[0], limits[1], 0.01);
			cout<<"\033[1;34mРезультат: "<<result<<"\n\033[0m";
	        }else
        	if (mode == "2"){
                        Function2* function = new Function2;
                        float* limits = new float[2];
                        read_limits(limits);
                        float result = calculate_integral(function, limits[0], limits[1], 0.01);
                        cout<<"\033[1;34mРезультат: "<<result<<"\n\033[0m";
	        }
        	else{
                	cout<<"\033[1;31mНекорректная функция!\n\033[0m";
	                print_modes();
        	}
	}
	bool read_number(float &num, float  min, float max){
        	string snum;
        	cin>>snum;
		bool right = false;
		try {
	        	num = stof(snum);
			right = num <= max && num >= min;
		}
		catch(invalid_argument& ia){};
        	if (!right){
                	cout<<"\033[1;31mНедопустимое значение параметра!\033[0m\n";
        	}
        	return right;
	}
    bool read_number(double &num, double  min, double max){
        string snum;
        cin>>snum;
        bool right = false;
        try {
            num = stof(snum);
            right = num <= max && num >= min;
        }
        catch(invalid_argument& ia){};
        if (!right){
            cout<<"\033[1;31mНедопустимое значение параметра!\033[0m\n";
        }
        return right;
    }
    bool read_number(long double &num, long double  min, long double max){
        string snum;
        cin>>snum;
        bool right = false;
        try {
            num = stof(snum);
            right = num <= max && num >= min;
        }
        catch(invalid_argument& ia){};
        if (!right){
            cout<<"\033[1;31mНедопустимое значение параметра!\033[0m\n";
        }
        return right;
    }
	void read_limits(float* limits){
		 bool checked = false;
                 while(!checked){
			cout<<"Введите нижний предел:\n";
			checked = read_number(limits[0], -100.0, 100.0);
                 }
                 checked = false;
                 while(!checked){
                        cout<<"Введите верхний предел:\n";
                        checked = read_number(limits[1], -100.0, 100.0);
                 }		
	} 

	template< class Class >	
	float calculate_integral(Class* Function, float down, float up, float delta){
		bool bigger_zero = up - down > 0;
		if (bigger_zero){
			float sum = 0, previous_sum;
			float lenght = up - down;
			int parts = 5;
			float part = lenght / parts;
			for(int i = 0; i < parts; i++){
				sum += (abs(Function->calculate(down+(i*part)))+abs(Function->calculate(down+((i+1)*part))))/2;
			}
			do {
				previous_sum = sum;
				sum = 0;
				parts *= 2;
				part = lenght / parts;
                  		for(int i = 0; i < parts; i++){
                                	sum += (Function->calculate(down+(i*part))+Function->calculate(down+((i+1)*part)))/2; 
                        	}
			} while(abs(previous_sum - sum) < delta);
			return sum;   
		}
		else {
			while(true){
				cout<<"\033[1;31mНижний предел интегрирования больше верхнего!\033[0m\n";
        	                cout<<"1)Поменять пределы\n";
				cout<<"2)Всеравно посчитать\n";
				string mode;
               			cin>>mode;
   				if (mode == "1"){
                        		return calculate_integral(Function, up, down, delta);
                		}else
	                	if (mode == "2"){
					return -calculate_integral(Function, up, down, delta);
                		}
                		else{
                        		cout<<"Некорректный ввод\n";
                		}
			}
		}
	}
}
