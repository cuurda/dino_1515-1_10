#include "CuurdaIntegrator.h"
#include "Function1.cpp"
#include "Function2.cpp"

namespace CuurdaIntegrator{
	void print_hello(){
        cout<<"Вас приветствует программа \"Расчет интегралов\"\n";
        print_modes();
	}
	void print_modes(){
        cout<<"Выберите функцию, чтобы посчитать определенный интеграл:\n";
        cout<<"\t1) f(x)=(sin(ax))^n*x^(m/k)\n";
        cout<<"\t2) f(x)=d*e^(-g*x^n)*x^(m/k)\n";
        select_function();
	}

	void select_function(){
        string mode;
        cin>>mode;
        if (mode == "1"){
            Function1* function = new Function1;
            prepare_function(function);
        }else
        if (mode == "2"){
            Function2* function = new Function2;
            prepare_function(function);
        }
        else{
            cout<<"\033[1;31mНекорректная функция!\n\033[0m";
            print_modes();
        }
	}
    template< class Class >
    void prepare_function(Class* function){
        cout<<"Веденная функция: "<<"\033[1;34m";
        function->print();
        cout<<"\033[0m";
        double* limits = new double[2];
        read_limits(limits);
        double result = calculate_integral(function, limits[0], limits[1], 0.01);
        cout<<"\033[1;34mИнтеграл функции: "<<result<<"\n\033[0m";
    }
    bool read_number(double &num, double  min, double max){
        string snum;
        cin>>snum;
        bool right = false;
        try {
            num = stof(snum);
            right = num <= max && num >= min;
        }
        catch(invalid_argument& ia){};
        if (!right){
            cout<<"\033[1;31mНедопустимое значение параметра!\033[0m\n";
        }
        return right;
    }
    
	void read_limits(double* limits){
        bool checked = false;
        while(!checked){
            cout<<"Введите нижний предел:\n";
			checked = read_number(limits[0], -100.0, 100.0);
        }
        checked = false;
        while(!checked){
            cout<<"Введите верхний предел:\n";
            checked = read_number(limits[1], -100.0, 100.0);
        }
	} 

 	template< class Class >
	double calculate_integral(Class* Function, double down, double up, double delta){
		bool up_bigger_down = up - down >= 0;
		if (up_bigger_down){
			double sum = 0.0, previous_sum=0.0;
			double lenght = up - down;
			int parts = 5;
			double part = lenght / parts;
            if (DEBUG) {
                cout<<lenght<<endl;
                cout<<part<<endl;
            }
			for(int i = 0; i < parts; i++){
                if (DEBUG){
                        print_debug_trape_info(Function, down+i*part, down+(i+1)*part, part);
                }
				sum += calculate_square(Function, down+i*part, down+(i+1)*part, part);
			}
			do {
				previous_sum = sum;
				sum = 0;
				parts *= 2;
				part = lenght / parts;
                if (DEBUG) {
                    cout<<lenght<<endl;
                    cout<<part<<endl;
                }
                for(int i = 0; i < parts; i++){
                    if (DEBUG){
                        print_debug_trape_info(Function, down+i*part, down+(i+1)*part, part);
                    }
                    sum += calculate_square(Function, down+i*part, down+(i+1)*part, part);
                }
			} while(abs(previous_sum - sum) > delta);
			return sum;   
		}
		else {
			while(true){
				cout<<"\033[1;31mНижний предел интегрирования больше верхнего!\033[0m\n";
                cout<<"1)Поменять пределы\n";
				cout<<"2)Всеравно посчитать\n";
				string mode;
                cin>>mode;
   				if (mode == "1"){
                    return calculate_integral(Function, up, down, delta);
                }else if (mode == "2"){
					return -calculate_integral(Function, up, down, delta);
                }
                else{
                    cout<<"Некорректный ввод\n";
                }
			}
		}
	}
    
    template< class Class >
    double calculate_square(Class* Function, double down, double up, double part ){
        return (
                    abs(Function->calculate(down))+
                    abs(Function->calculate(up))
                )/2*part;
    }
    
    template< class Class >
    void print_debug_trape_info(Class* Function, double down, double up, double part ){
        cout
        <<"("
        <<abs(Function->calculate(down))
        <<"+"
        <<abs(Function->calculate(up))
        <<")/2*"
        <<part
        <<"="
        <<calculate_square(Function, down, up, part)
        <<endl;
    }
}
