#include <iostream>
#include <string>
#include <iomanip>
#include "../../CuurdaIntegrator.cpp"

using namespace CuurdaIntegrator;
using namespace std;
int main(int argc, char* argv[]){
	if (argc == 6){
		//cout << argv[1] << " " <<  argv[2]  << " " <<  argv[3] << " " <<  argv[4]  << " " <<  argv[5] << endl;
		string a(argv[1]);
		string n(argv[2]);
 		string m(argv[3]);
		string k(argv[4]);  
		string x(argv[5]);  
		Function1* function = new Function1(stod(a), stod(n), stod(m), stod(k));
		double res = function->calculate(stod(x));
        std::cout << std::fixed << std::setprecision(12)<<res;
	}
	return 0;
} 
