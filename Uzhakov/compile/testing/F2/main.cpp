#include <iostream>
#include <string>
#include <iomanip>
#include "../../CuurdaIntegrator.cpp"

using namespace CuurdaIntegrator;
using namespace std;
int main(int argc, char* argv[]){
	if (argc == 7){
		//cout << argv[1] << " " <<  argv[2]  << " " <<  argv[3] << " " <<  argv[4]  << " " <<  argv[5] << endl;
		string d(argv[1]);
		string g(argv[2]);
        string n(argv[3]);
 		string m(argv[4]);
		string k(argv[5]);
		string x(argv[6]);
		Function2* function = new Function2(stod(d), stod(g), stod(n), stod(m), stod(k));
		double res = function->calculate(stod(x));
        std::cout << std::fixed << std::setprecision(12)<<res;
	}
	return 0;
} 
