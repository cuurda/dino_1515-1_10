import subprocess
import random
import csv
with open("output_d.csv",'w') as resultFile:
	wr = csv.writer(resultFile, delimiter="	")
	for i in range(-500,200):
		d = 1#random.randint(0, 10000)/100
		g = 0#random.randint(0, 10000)/100
		n = 1#random.randint(0, 10000)/100
		m = 2#random.randint(0, 10000)/100
		k = 2#random.randint(0, 10000)/100
		x = i/100
		out = subprocess.run(['./app', str(d), str(g), str(n), str(m), str(k), str(x)], stdout=subprocess.PIPE)
		print(out.stdout)
		line= ["{:10.2f}".format(d).replace('.', ','),
               "{:10.2f}".format(g).replace('.', ','),
               "{:10.2f}".format(n).replace('.', ','),
               "{:10.2f}".format(m).replace('.', ','),
               "{:10.2f}".format(k).replace('.', ','),
               "{:10.2f}".format(x).replace('.', ','),
               out.stdout.decode('utf-8').replace('.', ','),
               ("=A"+str(i+501)+"*EXP(-B"+str(i+501)+"*СТЕПЕНЬ(F"+str(i+501)+";C"+str(i+501)+"))*СТЕПЕНЬ(F"+str(i+501)+";D"+str(i+501)+"/E"+str(i+501)+")"),
               ("=ABS(H"+ str(i+501) + "- G"+ str(i+501) + ")")]
		wr.writerow(line)
