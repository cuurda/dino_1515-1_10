import subprocess
import random
import csv
with open("output_d.csv",'w') as resultFile:
    wr = csv.writer(resultFile, delimiter="	")
    str_num = 1
    for i in range(0,1000):
        for j in range(i,1000):
            down_limit = i / 10
            up_limit = j / 10
            out = subprocess.run(['./app', str(down_limit), str(up_limit)], stdout=subprocess.PIPE)
            print(out.stdout)
            line= ["{:10.2f}".format(down_limit).replace('.', ','),
                   "{:10.2f}".format(up_limit).replace('.', ','),
                   out.stdout.decode('utf-8').replace('.', ','),
                   "=СТЕПЕНЬ(B" + str(str_num) + ";3)/3-СТЕПЕНЬ(A" + str(str_num) + ";3)/3",
                   "=ABS(C" + str(str_num) + " - D" + str(str_num) + ")"]
            wr.writerow(line)
            str_num+=1
