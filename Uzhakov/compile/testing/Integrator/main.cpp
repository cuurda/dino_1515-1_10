#include <iostream>
#include <string>
#include <iomanip>
#include "../../CuurdaIntegrator.cpp"
#include "Function3.cpp"

using namespace CuurdaIntegrator;
using namespace std;
int main(int argc, char* argv[]){
	if (argc == 3){
		string down_limit(argv[1]);
        string up_limit(argv[2]);
		Function3* function = new Function3();
        double res = calculate_integral(function, stod(down_limit), stod(up_limit), 0.001);
        std::cout << std::fixed << std::setprecision(12)<<res;
	}
	return 0;
} 
