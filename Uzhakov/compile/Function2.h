namespace CuurdaIntegrator{
	class Function2 { //f(x)=d*e^(-g*x^n)*x^(m/k)
		private:
            double m_d;
            double m_g;
            double m_n;
            double m_m;
            double m_k;
		public:
			Function2();
            Function2(double d, double g, double n, double m, double k);
			double calculate(double x);
            void print();
	};
}

