#include <iostream>
using namespace std;
namespace CuurdaIntegrator {
    bool DEBUG = false;
    
    template< class Class >
        double calculate_integral(Class* Function, double down, double up, double delta);
    template< class Class >
        double calculate_square(Class* Function, double down, double up, double delta);
    template< class Class >
        void print_debug_trape_info(Class* Function, double down, double up, double part );
    template< class Class >
        void prepare_function(Class* Function);
    
    void print_modes();
    void select_function();
    void print_hello();
	bool read_number(double &num, double min, double max);
    void read_limits(double* limits);

    class Function1;
	class Function2;
}
