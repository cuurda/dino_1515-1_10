#include <string>
#include <cmath>

#include "Function2.h"

namespace CuurdaIntegrator{
	Function2::Function2(){
			bool checked = false;
			while(!checked){
				cout<<"Введите d:\n";
				checked = read_number(m_d, -100.0, 100.0);
			}
			checked = false;
			while(!checked){
				cout<<"Введите g:\n";
				checked = read_number(m_g, -100.0, 100.0);
            }
            checked = false;
            while(!checked){
                cout<<"Введите n:\n";
                checked = read_number(m_n, -100.0, 100.0);
            }
			checked = false;
			while(!checked){
				cout<<"Введите m:\n";
				checked = read_number(m_m, -100.0, 100.0);
			}
			checked = false;
			while(!checked){
                cout<<"Введите k:\n";
                checked = read_number(m_k, -100.0, 100.0);
            }
		};
    Function2::Function2(double d, double g, double n, double m, double k){
        m_d = d;
        m_g = g;
        m_n = n;
        m_m = m;
        m_k = k;
    }
	double Function2::calculate(double x){
		return m_d*
            exp(-m_g*pow(x,m_n))
            *pow(x,m_m/m_k);
	};
    void Function2::print(){
        cout<<m_d<<"exp("<<-m_g<<"*x^"<<m_n<<")*x^("<<m_m<<"/"<<m_m<<")";
    }
}

