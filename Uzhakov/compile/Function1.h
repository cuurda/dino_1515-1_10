namespace CuurdaIntegrator{
	class Function1 { //f(x)=(sin(ax))^n*x^(m/k)
		private:
            double m_a;
            double m_n;
            double m_m;
            double m_k;
		public:
			Function1();
            Function1(double a, double n, double m, double k);
			double calculate(double x);
            void print();
	};
}

