#include <string>
#include <cmath>

#include "Function1.h"

namespace CuurdaIntegrator{
	Function1::Function1(){
			bool checked = false;
			while(!checked){
				cout<<"Введите a:\n";
				checked = read_number(m_a, -100.0, 100.0);
			}
			checked = false;
			while(!checked){
				cout<<"Введите n:\n";
				checked = read_number(m_n, -100.0, 100.0);
            }
			checked = false;
			while(!checked){
				cout<<"Введите m:\n";
				checked = read_number(m_m, -100.0, 100.0);
			}
			checked = false;
			while(!checked){
                cout<<"Введите k:\n";
                checked = read_number(m_k, -100.0, 100.0);
            }
		};
    Function1::Function1(double a, double n, double m, double k){
        m_a = a;
        m_n = n;
        m_m = m;
        m_k = k;
    };
	double Function1::calculate(double x){
		return pow(sin(m_a*x),m_n)*pow(x,m_m/m_k);
	};
    
    void Function1::print(){
        cout<<"(sin("<<m_a<<"x)"<<"^"<<m_n<<")*"`<<"x^("<<m_m<<"/"<<m_k<<")"<<endl;
    }
}

