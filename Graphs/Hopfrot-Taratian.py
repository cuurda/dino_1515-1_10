class vertice(object):
    def __init__(self):
        self.edges = []
        self.marked = False
        self.parrent = None
        self.tin = None
        self.fup = None
    def add_neibour(self, x):
        self.edges.append(x)
    def __str__(self):
        return str(self.edges) + " " + str(self.marked)+ " " + str(self.parrent)


vertices = []
vertices_str = []
cutpoints = []
comp = 0
timer = 0

v_pow, e_pow = input().split()
def dfs(num, parrent = -1):
    global vertices
    global timer
    global cutpoints
    vertices[num].marked = True
    vertices[num].parrent = parrent
    vertices[num].tin = timer
    vertices[num].fup = timer
    timer += 1
    children = 0
    elen = len(vertices[num].edges)
    for to in vertices[num].edges:
        if to == parrent:
            continue
        if vertices[to].marked:
            if vertices[num].fup > vertices[to].tin:
                vertices[num].fup = vertices[to].tin
        else:
            dfs(to, num)
            if vertices[num].fup > vertices[to].fup:
                vertices[num].fup = vertices[to].fup
            if vertices[to].fup >= vertices[num].tin and not (parrent == -1):
                try:
                    cutpoints.remove(num)
                except ValueError:
                    pass
                cutpoints.append(num+1)
            children += 1
    if parrent == -1 and children > 1:
        try:
            cutpoints.remove(num)
        except ValueError:
            pass
        cutpoints.append(num+1)

for i in range(0,int(v_pow)):
    vertices.append(vertice())

for i in range(0,int(e_pow)):
    fv, sv = input().split()
    fv = int(fv)
    sv = int(sv)
    vertices[fv-1].add_neibour(sv-1)
    vertices[sv-1].add_neibour(fv-1)

dfs(0)
res = ""
for i in range(0,len(cutpoints)):
    res += str(cutpoints[i]) + " "

print(res)