from queue import Queue
class vertice(object):
    def __init__(self):
        self.edges = []
        self.stage = 0
        self.marked = False
        #self.parrent = None
    def add_neibour(self, x):
        self.edges.append(x)
    def __str__(self):
        return str(self.edges)  + " " + str(self.marked)+ " " + str(self.stage)

v_pow, e_pow = input().split()
vertices = []
vertices_str = []
vertices_que = Queue()

for i in range(0,int(v_pow)):
    vertices.append(vertice())

for i in range(0,int(e_pow)):
    fv, sv = input().split()
    fv = int(fv)
    sv = int(sv)
    vertices[fv].add_neibour(sv)
    vertices[sv].add_neibour(fv)

vertices[0].marked = True
vertices_que.put(vertices[0])

while not vertices_que.empty():
    curr_vertice = vertices_que.get()

    stage = curr_vertice.stage
    for v in curr_vertice.edges:
        if not vertices[v].marked:
            vertices[v].stage = stage + 1
            vertices[v].marked = True
            vertices_que.put(vertices[v])

result = ""
for i in range(0,int(v_pow)):
    result += str(vertices[i].stage) + " "
    vertices_str.append(str(vertices[i]))

#print(vertices_str)
print(result)