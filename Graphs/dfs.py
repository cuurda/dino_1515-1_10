class vertice(object):
    def __init__(self):
        self.edges = []
        self.marked = False
        self.parrent = None
    def add_neibour(self, x):
        self.edges.append(x)
    def __str__(self):
        return str(self.edges) + " " + str(self.marked)+ " " + str(self.parrent)


v_pow, e_pow = input().split()
vertices = []
vertices_str = []
comp = 0
def dfs(num, parrent):
    global vertices
    vertices[num].marked = True
    vertices[num].parrent = parrent
    elen = len(vertices[num].edges)
    for i in range(0, elen):
        if (not vertices[vertices[num].edges[i]].marked):
            dfs(vertices[num].edges[i], num)
        else:
            pass

def found_comp():
    global vertices
    global comp
    for i in range(0, int(v_pow)):
        if not vertices[i].marked:
            dfs(i, None)
            comp+=1

for i in range(0,int(v_pow)):
    vertices.append(vertice())

for i in range(0,int(e_pow)):
    fv, sv = input().split()
    fv = int(fv)
    sv = int(sv)
    vertices[fv-1].add_neibour(sv-1)
    vertices[sv-1].add_neibour(fv-1)

found_comp()
for i in range(0,int(v_pow)):
    vertices_str.append(str(vertices[i]))

print(comp)