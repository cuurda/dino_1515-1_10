import random

def randomgraph(numvertex):
	matrix = []
	for i in range(numvertex+1):
		matrix.append([])
	matrix[0].append(" ")
	for i in range(1,numvertex+1):
		matrix[0].append(chr(96+i))
	for i in range(1, numvertex+1):
		matrix[i].append(chr(96+i)) 
		for j in range(1, numvertex+1):			
			matrix[i].append([0,random.randint(0,15),random.randint(0,15)][random.randint(0,2)])
	string=""
	string+="\n"
	for j in range(numvertex+1):
		for i in range(numvertex+1):
			string+="|"+'{0:2}'.format(str(matrix[j][i]))
		string+="\n"
	string+="\n"
	print(string)		

vertices=int(input())
num = int(input())
for i in range(num):
	print(i+1)
	randomgraph(vertices)
