class StackMax:
    stack = []
    max_stack = []
    def push(self, num):
        self.stack.append(num)
        if len(self.max_stack) > 0:
            if self.max_stack[-1] > num:
                self.max_stack.append(self.max_stack[-1])
            else:
                self.max_stack.append(num)
        else:
            self.max_stack.append(num)

    def pop(self):
        self.stack.pop()
        self.max_stack.pop()

    def max(self):
        return self.max_stack[-1]

operations_num = int(input())
max_stack = StackMax()
for i in range(operations_num):
    command = input().split()
    if command[0] == "push":
        max_stack.push(int(command[1]))
    elif command[0] == "pop":
        max_stack.pop()
    elif command[0] == "max":
        print(max_stack.max())