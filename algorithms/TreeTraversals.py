class BinaryTree:
    def __init__(self, lenght ):
        self.keys = [ -1 for i in range(lenght)]
        self.left_children = [ -1 for i in range(lenght)]
        self.right_children = [ -1 for i in range(lenght)]
        for i in range(lenght):
            inputed = input().split()
            self.keys[i] = int(inputed[0])
            self.left_children[i] = int(inputed[1])
            self.right_children[i] = int(inputed[2])

    def in_order(self, root=0):
        result = []
        if not self.left_children[root] == -1:
            result.extend(self.in_order(self.left_children[root]))
        result.append(self.keys[root])
        if not self.right_children[root] == -1:
            result.extend(self.in_order(self.right_children[root]))
        return result

    def pre_order(self, root=0):
        result = []
        result.append(self.keys[root])
        if not self.left_children[root] == -1:
            result.extend(self.pre_order(self.left_children[root]))
        if not self.right_children[root] == -1:
            result.extend(self.pre_order(self.right_children[root]))
        return result

    def post_order(self, root=0):
        result = []
        if not self.left_children[root] == -1:
            result.extend(self.post_order(self.left_children[root]))
        if not self.right_children[root] == -1:
            result.extend(self.post_order(self.right_children[root]))
        result.append(self.keys[root])
        return result

lenght = int(input())
bt = BinaryTree(lenght)
print(" ".join(str(elem) for elem in bt.in_order()))
print(" ".join(str(elem) for elem in bt.pre_order()))
print(" ".join(str(elem) for elem in bt.post_order()))