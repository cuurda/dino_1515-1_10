import sys
sys.setrecursionlimit(100000)
class BinaryTree:
    def __init__(self, lenght ):
        self.keys = [ -1 for i in range(lenght)]
        self.left_children = [ -1 for i in range(lenght)]
        self.right_children = [ -1 for i in range(lenght)]
        self.max = None
        self.min = None
        for i in range(lenght):
            inputed = input().split()
            self.keys[i] = int(inputed[0])
            self.left_children[i] = int(inputed[1])
            self.right_children[i] = int(inputed[2])
            if self.max is None:
                self.max = self.keys[i]
            else:
                if self.max < self.keys[i]:
                    self.max = self.keys[i]
            if self.min is None:
                self.min = self.keys[i]
            else:
                if self.min > self.keys[i]:
                    self.min = self.keys[i]
                    
    def check(self, root=0):
        left_true = True
        right_true = True
        if len(self.keys) > 0:
            if not self.left_children[root] == -1:
                left_true = self.check_not_root(self.left_children[root], self.min, self.keys[root])
            if not self.right_children[root] == -1:
                right_true = self.check_not_root(self.right_children[root], self.keys[root], self.max)
        return left_true and right_true

    def check_not_root(self, root, min_, max_ ):
        if min_ <= self.keys[root] <= max_:
            left_true = True
            right_true = True
            if not self.left_children[root] == -1:
                left_true = self.check_not_root(self.left_children[root], min_, self.keys[root])
            if not self.right_children[root] == -1:
                right_true = self.check_not_root(self.right_children[root], self.keys[root], max_)
            return left_true and right_true
        else:
            return False

lenght = int(input())
bt = BinaryTree(lenght)
if bt.check():
    print("CORRECT")
else:
    print("INCORRECT")