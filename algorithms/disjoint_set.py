class DisjointSets:
    def __init__(self, num_sets, str_values, maxim):
        self._sets = [i for i in range(num_sets)]
        self._ranks = [0 for i in range(num_sets)]
        self._values = []
        maxim.append(int(str_values[0]))
        for i in range(num_sets):
            self._values.append(int(str_values[i]))
            if self._values[i] > maxim[0]:
                maxim[0] = self._values[i]
        #print(self._sets)
        #print(self._values)

    def find(self,a):
        start = a
        while not a == self._sets[a]:
            a = self._sets[a]
        while not start == self._sets[start]:
            temp = self._sets[start]
            self._sets[start] = a
            start = self._sets[temp]
        return a

    def union(self, a, b):
        id_a = self.find(a)
        id_b = self.find(b)
        if id_a == id_b:
            return self._values[id_a]
        if self._ranks[id_a] > self._ranks[id_b]:
            self._sets[id_b] = self._sets[id_a]
            self._values[id_a] += self._values[id_b]
            self._values[id_b] = -1
            return self._values[id_a]
        else:
            self._sets[id_a] = self._sets[id_b]
            if self._ranks[id_a] == self._ranks[id_b]:
                self._ranks[id_b]+=1
            self._values[id_b] += self._values[id_a]
            self._values[id_a] = -1
            return self._values[id_b]
inputed = input().split()
lenght  = int(inputed[0])
queries = int(inputed[1])
if lenght > 0:
    values  = input().split()
    maxim = []
    disjoint_sets = DisjointSets(lenght, values, maxim)
    maxim = maxim[0]
    for i in range(queries):
        inputed = input().split()
        geted = disjoint_sets.union(int(inputed[0]) - 1, int(inputed[1]) - 1)
        maxim = maxim if maxim > geted else geted
        print(maxim)