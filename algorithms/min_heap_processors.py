class MinHeap:
    def __init__(self, lenght):
        self.values = [0 for i in range(lenght)]
        self.processors = [i for i in range(lenght)]
        self.lenght = lenght

    def print(self):
        print(self.processors)
        print(self.values)

    def _parrent(self, i):
        parrent = int(i/2)
        return None if parrent == i else parrent

    def _children(self, i):
        left = None
        right = None
        if 2 * i + 1 < self.lenght:
            left = 2 * i + 1
        if 2 * i + 2 < self.lenght:
            right = 2 * i + 2
        return left, right

    def _min_child(self, i):
        chidren = self._children(i)
        if chidren[0] == None:
            return None
        elif chidren[1] == None:
            return chidren[0]
        else:
            if not self.values[chidren[0]] == self.values[chidren[1]]:
                return chidren[0] if self.values[chidren[0]] < self.values[chidren[1]] else chidren[1]
            else:
                return chidren[0] if self.processors[chidren[0]] < self.processors[chidren[1]] else chidren[1]

    def _swift_down(self, i):
        min_child = self._min_child(i)
        if not min_child == None: 
            while self.values[min_child] <= self.values[i]:
                if self.values[min_child] == self.values[i] and self.processors[min_child] < self.processors[i]:
                    #print(str(i) + " " + str(min_child))
                    temp = self.values[i]
                    temp2 = self.processors[i]
                    self.values[i] = self.values[min_child]
                    self.processors[i] = self.processors[min_child]
                    self.values[min_child] = temp
                    self.processors[min_child] = temp2
                    i = min_child
                    min_child = self._min_child(i)
                    if min_child == None:
                        break
                elif self.values[min_child] < self.values[i]:
                    temp = self.values[i]
                    temp2 = self.processors[i]
                    self.values[i] = self.values[min_child]
                    self.processors[i] = self.processors[min_child]
                    self.values[min_child] = temp
                    self.processors[min_child] = temp2
                    i = min_child
                    min_child = self._min_child(i)
                    if min_child == None:
                        break
    def get_root(self):
        return self.processors[0], self.values[0]

    def change_root(self, value):
        self.values[0] = value
        self._swift_down(0)


inputed = input().split()
processors = int(inputed[0])
lenght = int(inputed[1])

data = input().split()

min_heap = MinHeap(processors)
for i in range(lenght):
    root = min_heap.get_root()
    print(root[0], root[1])
    min_heap.change_root(root[1] + int(data[i]))
    #min_heap.print()