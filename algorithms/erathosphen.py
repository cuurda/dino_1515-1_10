import math
def eratosphen(num):
	numbers = []
	bits = []
	for i in range(0,num):
		bits.append(False)
	period = 2
	while period * period <= num:
		for i in range(period+period, num+1, period): 
			bits[i-1] = True
		period += 1
		while period < num and bits[period-1]:
			period += 1
	for i in range(0,num):
		if not bits[i]:
			numbers.append(i+1)
	return numbers

def razlozh(num):
	logs = []
	nums = []
	if num>100:
		resheto = eratosphen(int(math.ceil(math.sqrt(num))))
	else:
		resheto = eratosphen(num)
	i = 1
	j = -1
	prep = 0
	while not num == 1 and i < len(resheto):
		p = resheto[i]
		if num % p == 0:
			if not prep == p:  
				nums.append(p)
				logs.append(1)
				j+=1
				prep = p
			else:
				logs[j] += 1
			num = num // p
		else:
			i += 1
	return nums, logs 

def lcm(num_tuple):
	lenght = len(num_tuple)
	nums = []
	logs = []
	for i in range(lenght):
		n, l = razlozh(num_tuple[i])
		for j in range(len(n)):
			try:
				c = nums.index(n[j])
				if logs[c] < l[j]:
					logs[c] = l[j]
			except ValueError:
				nums.append(n[j])
				logs.append(l[j])
		print(nums)
		print(logs)
	lcmn = 1
	for i in range(len(nums)):
		lcmn *= nums[i]**logs[i]
	return lcmn

#a = int(input())
#print(eratosphen(a))
#print(razlozh(a))
print(lcm([35,63,11]))

