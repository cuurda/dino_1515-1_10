class Node:
    def __init__(self, key, left, right):
        self.key = key
        self.parrent = None
        self.left = left
        self.right = right
        self.sum = key

class SplayTree:
    def __init__(self):
        pass

    def set_parrent(self, child, parrent):
        if not child is None:
            child.parrent = parrent
            if not parrent.left is None:
                if not parrent.right is None:
                    parrent.sum = parrent.key + parrent.left.sum + parrent.right.sum
                else:
                    parrent.sum = parrent.key + parrent.left.sum
            else:
                if not parrent.right is None:
                    parrent.sum = parrent.key + parrent.right.sum
                else:
                    parrent.sum = parrent.key
    def keep_parrent(self, v):
        self.set_parrent(v.left, v)
        self.set_parrent(v.right, v)

    def rotate(self, parrent, child):
        gparrent = parrent.parrent
        if not gparrent is None:
            if gparrent.left == parrent:
                gparrent.left = child
            else:
                gparrent.right = child
        if parrent.left == child:
            parrent.left, child.right = child.right, parrent
        else:
            parrent.right, child.left = child.left, parrent

        self.keep_parrent(child)
        self.keep_parrent(parrent)
        child.parrent = gparrent

    def splay(self, v):
        if v.parrent is None:
            return v
        parrent = v.parrent
        gparrent = parrent.parrent
        if gparrent is None:
            self.rotate(parrent, v)
            return v
        else:
            zigzig = (gparrent.left == parrent) == (parrent.left == v)
            if zigzig:
                self.rotate(gparrent, parrent)
                self.rotate(parrent, v)
            else:
                self.rotate(parrent, v)
                self.rotate(gparrent, v)
        return self.splay(v)

    def find(self, v, key):
        if v is None:
            return None
        if key == v.key:
            return self.splay(v)
        if key < v.key and not v.left is None:
            return self.find(v.left, key)
        if key > v.key and not v.right is None:
            return self.find(v.right, key)
        return self.splay(v)

    def split(self, root, key):
        if root is None:
            return None, None
        root = self.find(root, key)
        if root.key == key:
            self.set_parrent(root.left, None)
            self.set_parrent(root.right, None)
            return root.left, root.right
        if root.key < key:
            right, root.right = root.right, None
            self.set_parrent(right, None)
            return root, right
        else:
            left, root.left = root.left, None
            self.set_parrent(left, None)
            return left, root

    def insert(self, root, key):
        left, right = self.split(root, key)
        root = Node(key, left, right)
        self.keep_parrent(root)
        return root

    def merge(self, left, right):
        if right is None:
            return left
        if left is None:
            return right
        right = self.find(right, left.key)
        right.left, left.parrent = left, right
        return right

    def remove(self, root, key):
        root = self.find(root, key)
        self.set_parrent(root.left, None)
        self.set_parrent(root.right, None)
        return self.merge(root.left, root.right)

splay_tree = SplayTree()
lenght = int(input())
root = None
for i in range(lenght):
    inputed = input().split()
    if inputed[0] == "+":
        root = splay_tree.insert(root, int(inputed[1]))
    if inputed[0] == "-":
        root = splay_tree.remove(root, int(inputed[1]))
    if inputed[0] == "?":
        founded = splay_tree.find(root, int(inputed[1]))
        if founded is None:
            print("Not found")
        elif int(inputed[1]) == founded.key:
            print("Found")
            print(founded.sum)
        else:
            print("Not found")