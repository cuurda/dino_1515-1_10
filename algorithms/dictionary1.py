num_commands = int(input())

dicts = dict()

for i in range(num_commands):
    command = input().split()
    if command[0] == "add":
        dicts.update({command[1] : command[2]})
    if command[0] == "del":
        dicts.pop(command[1],None)
    if command[0] == "find":
        print(dicts.get(command[1], "not found"))