import re
string = input()
string2 = ""
where_bracket = []
for i in range(len(string)):
    if string[i] == "{" or string[i] == "[" or string[i] == "(" or string[i] == ")" or string[i] == "]" or string[i] == "}":
        string2 += string[i]
        where_bracket.append(i)
stack = []
data = -1
first_open = -1
for i in range(len(string2)):
    if string2[i] == "{" or string2[i] == "[" or string2[i] == "(":
        stack.append(string2[i])
        if first_open == -1:
            first_open = i
    elif len(stack) > 0:
        brack = stack.pop()
        if brack == "{" and string2[i] == "}" or brack == "[" and string2[i] == "]" or brack == "(" and string2[i] == ")":
            if len(stack) == 0:
                first_open = -1
        else:
            data = i
            break
    else:
        data = i
        break
if data != -1:
    print(where_bracket[data]+1)
else:
    if first_open == -1:
        print("Success")
    else:
        print(where_bracket[first_open]+1)