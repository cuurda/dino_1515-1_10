class PairStack:
    stack1 = []
    stack2 = []
    def push(self, num1, num2):
        self.stack1.append(num1)
        self.stack2.append(num2)
    def pop(self):
        return self.stack1.pop(), self.stack2.pop()

    def top1(self):
        return self.stack1[-1]

    def top2(self):
        return self.stack2[-1]

    def empty(self):
        return len(self.stack1) == 0

    def __len__(self):
        return len(self.stack1)

    def print(self):
        print(self.stack1, self.stack2)


class StackMax:
    pair_stack = PairStack()
    def push(self, num):
        if not self.pair_stack.empty():
            maxim = self.pair_stack.top2()
            if maxim < num:
                self.pair_stack.push(num, num)
            else:
                self.pair_stack.push(num, maxim)
        else:
            self.pair_stack.push(num, num)

    def pop(self):
        return self.pair_stack.pop()

    def max(self):
        return self.pair_stack.top2()

    def __len__(self):
        return len(self.pair_stack)

    def print(self):
        self.pair_stack.print()


class MyQueue:
    in_stack = []
    in_max = 0
    out_stack = StackMax()

    def push(self, num):
        self.in_stack.append(num)
        if self.in_max < num:
            self.in_max = num

    def pop(self):
        if len(self.out_stack) > 0:
            return self.out_stack.pop()
        else:
            while len(self.in_stack) > 0:
                self.out_stack.push(self.in_stack.pop())
                self.in_max = 0
            return self.out_stack.pop()

    def max(self):
        poped = my_queue.pop()[1]
        return poped if poped > self.in_max else self.in_max

    def print(self):
        print(self.in_stack)
        self.out_stack.print()

my_queue = MyQueue()
lenght = int(input())
num_list = input().split()
window = int(input())
pointer = window
#print(num_list)
#print("------------------")

for i in range(window):
    my_queue.push(int(num_list[i]))
    #my_queue.print()
    #print("------------------")

while pointer < lenght:
    print(my_queue.max())
    #my_queue.print()
    #print("------------------")
    my_queue.push(int(num_list[pointer]))
    pointer += 1

print(my_queue.max())