class DisjointSets:
    def __init__(self, num_sets):
        self._sets = [i for i in range(num_sets)]
        self._ranks = [0 for i in range(num_sets)]
        self._values = [1 for i in range(num_sets)]
    def find(self,a):
        start = a
        while not a == self._sets[a]:
            a = self._sets[a]
        while not start == self._sets[start]:
            temp = self._sets[start]
            self._sets[start] = a
            start = self._sets[temp]
        return a

    def union(self, a, b):
        id_a = self.find(a)
        id_b = self.find(b)
        if id_a == id_b:
            return False, self._values[id_a]
        if self._ranks[id_a] > self._ranks[id_b]:
            self._sets[id_b] = self._sets[id_a]
            self._values[id_a] += self._values[id_b]
            self._values[id_b] = -1
            return True, self._values[id_a]
        else:
            self._sets[id_a] = self._sets[id_b]
            if self._ranks[id_a] == self._ranks[id_b]:
                self._ranks[id_b]+=1
            self._values[id_b] += self._values[id_a]
            self._values[id_a] = -1
            return True, self._values[id_b]

def partition(list_edges_start, list_edges_end, list_edges_weight, start, end):
    pivot = list_edges_weight[int((start+end)/2)]
    i = start
    j = end§
def qsort(list_edges_start, list_edges_end, list_edges_weight, start, end):
    if start < end:
        pivot = partition(list_edges_start, list_edges_end, list_edges_weight)
        qsort(list_edges_start, list_edges_end, list_edges_weight start)
inputed = input().split()
vertices  = int(inputed[0])
edges = int(inputed[1])
list_edges_start = []
list_edges_end = []
list_edges_weight = []
list_correct_edge = []

if vertices > 0:
    disjoint_sets = DisjointSets(vertices)
    for i in range(edges):
        edge = input().split()
        list_edges_start.append(int(edge[0]))
        list_edges_end.append(int(edge[1]))
        list_edges_weight.append(int(edge[2]))

    print(list_edges_start)
    print(list_edges_end)
    print(list_edges_weight)

    qsort(list_edges_start, list_edges_end, list_edges_weight, 0, len(list_edges_weight)-1)

    print(list_edges_start)
    print(list_edges_end)
    print(list_edges_weight)

    for i in range(len(list_edges_weight)):
        weight = list_edges_weight.pop()
        start = list_edges_start.pop()
        end = list_edges_end.pop()
        result = disjoint_sets.union(start, end)
        if result[0]:
            list_correct_edge.append((start, end, weight))
            if result[1] == vertices:
                break

    print(list_correct_edge)