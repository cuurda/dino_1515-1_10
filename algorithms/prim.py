import sys
class vertice(object):
    def __init__(self):
        self.edges = []
        self.edges_weight = [] 
        self.marked = False
        self.parrent = None
    def add_neibour(self, x, we):
        self.edges.append(x)
        self.edges_weight.append(we)
    def __str__(self):
        return str(self.edges) + " " + str(self.marked)+ " " + str(self.parrent)+ " " + str(len(self.edges))

def matrix(vertices):
        mtx = []
        for i in range(0,len(vertices)):
            mtx.append([0 for g in range(len(vertices))])
            for j in range(0,len(vertices)):
                try:
                    mtx[i][j] = vertices[i].edges_weight[vertices[i].edges.index(j)]
                except ValueError:
                    mtx[i][j] = 0
        #print(mtx)
        return mtx

v_pow, e_pow = input().split()

vertices = []

for i in range(0,int(v_pow)):
    vertices.append(vertice())

for i in range(0,int(e_pow)):
    fv, sv, we = input().split()
    fv = int(fv)
    sv = int(sv)
    we = int(we)
    vertices[fv-1].add_neibour(sv-1, we)
    vertices[sv-1].add_neibour(fv-1, we)


mtx = matrix(vertices)
bits = [False for i in range(len(mtx))]

sum = 0
elem = sys.maxsize
ei, ej = 0, 0
tree = []

for i in range(len(mtx)):
    for j in range(len(mtx)):
        if mtx[i][j] != 0 and mtx[i][j] < elem:
            elem = mtx[i][j]
            ei = i
            ej = j
bits[ei] = True
bits[ej] = True
sum += mtx[ei][ej]
tree.append([ei+1, ej+1, mtx[ei][ej]])
print([ei+1, ej+1, mtx[ei][ej]])

elem = sys.maxsize
ei, ej = 0, 0

processing = False
for bit in bits:
    if not bit:
        processing = True

while processing:
    for i in range(len(mtx)):
        if bits[i] == True:
            for j in range(len(mtx)):
                if not bits[j] == True:
                    if mtx[i][j] != 0 and mtx[i][j] < elem:
                        elem = mtx[i][j]
                        ei = i
                        ej = j
    bits[ei] = True
    bits[ej] = True
    sum += mtx[ei][ej]
    tree.append([ei+1, ej+1, mtx[ei][ej]])
    print([ei+1, ej+1, mtx[ei][ej]])
    elem = sys.maxsize
    ei, ej = 0, 0
    processing = False
    for bit in bits:
        if not bit:
            processing = True

print(sum)
#print(bits)
#print(elem)
#print(ei)
#print(ej)
#print(tree)