# put your python code here
import sys
import array
sys.setrecursionlimit(100000)

mas_len = int(input())
mas_list = input().split()
mas_dict = { a: [] for a in range(-1, mas_len)}

for i in range(mas_len):
    mas_dict[int(mas_list[i])].append(i)

def count_tree_deep(root, mas_dict, mas_len):
    deep = 1
    for i in range(len(mas_dict[root])):
        sub_deep = 1 + count_tree_deep(mas_dict[root][i], mas_dict, mas_len)
        deep = (deep if sub_deep < deep else sub_deep)
    return deep

print(count_tree_deep(-1,mas_dict,mas_len)-1)
