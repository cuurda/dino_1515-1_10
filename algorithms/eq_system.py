class DisjointSets:
    def __init__(self, num_sets):
        self._sets = [i for i in range(num_sets)]
        self._ranks = [0 for i in range(num_sets)]
        #print(self._sets)

    def find(self,a):
        start = a
        while not a == self._sets[a]:
            a = self._sets[a]
        while not start == self._sets[start]:
            temp = self._sets[start]
            self._sets[start] = a
            start = self._sets[temp]
        return a

    def union(self, a, b):
        id_a = self.find(a)
        id_b = self.find(b)
        if self._ranks[id_a] > self._ranks[id_b]:
            self._sets[id_b] = self._sets[id_a]
        else:
            self._sets[id_a] = self._sets[id_b]
            if self._ranks[id_a] == self._ranks[id_b]:
                self._ranks[id_b] += 1

inputed = input().split()
lenght  = int(inputed[0])
equalities = int(inputed[1])
unequalities = int(inputed[2])
correct = 1
if lenght > 0:
    disjoint_sets = DisjointSets(lenght)
    for i in range(equalities):
        inputed = input().split()
        disjoint_sets.union(int(inputed[0]) - 1, int(inputed[1]) - 1)
    for i in range(unequalities):
        inputed = input().split()
        if not correct == 0:
            correct = 0 if disjoint_sets.find(int(inputed[0]) - 1) == disjoint_sets.find(int(inputed[1]) - 1) else 1
print(correct)