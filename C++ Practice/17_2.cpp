#include <iostream>
#include "prandom.cpp"
using namespace std;

int main(){
    prandom_init();
    int** dmas= new int*[10];
    for ( int i = 0; i<10; i++){
        dmas[i] = new int[10];
    }
    int s;
    for (int i = 0; i<10; i++){
        for (int k = 0; k<10; k++) {
            dmas[i][k] = prandom(30);
            cout << dmas[i][k] << " ";
        }
        cout<<endl;
    }
    cout<<endl;
    cout<<"Номер строки: ";
    cin>>s;
    for (int k = 0; k<10; k++) {
        cout << dmas[s][k] << " ";
    }
    cout<<endl;
    delete[] *dmas;
    delete[] dmas;
    return 0;
}
