#include <iostream>
#include <iomanip>

using namespace std;

int main() {
	double comp=1;
	
	for (int i = 2;i<=10;i++){
		comp*=(i+1/i);
	}
	cout<<setprecision(0)<<fixed;	
	cout<<comp;
}
