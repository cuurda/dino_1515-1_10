#include <iostream>
#include "prandom.cpp"
using namespace std;

struct House {
    char sername[50];
    float area;
    int numrooms;
    int floor;
};

int main(){
    int q30 = 0;
    prandom_init();
    House list_house[5];
    for (int i=0; i<5; i++) {
        prandom_string(list_house[i].sername, prandom(10,30));
        list_house[i].area = prandom(2000,10000)/100.0;
        list_house[i].numrooms = prandom(1,5);
        list_house[i].floor = prandom(1,30);
    }
    
    for (int i=0; i<5; i++) {
        cout << list_house[i].sername << "  "<< list_house[i].area << " " << list_house[i].numrooms << " " << list_house[i].floor << "\n";
    }
    cout<<"\n"<<"\n";
    
    for (int i=0; i<5; i++) {
        if (list_house[i].area < 30.0){
            cout << list_house[i].sername << "  " << " " << list_house[i].numrooms << " " << list_house[i].floor << "\n";
            q30++;
        }
    }
    cout<<q30<<"\n";
    return 0;
}
