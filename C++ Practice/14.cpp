#include <iostream>
#include <cstdio>

using namespace std;
int main(){
    
    char byte1 = 'a';
    char byte2 = 'A';
    int a1 = 12345;
    int a2 = 54321;
    float f1 = 12.345;
    float f2 = 54.321;
    
    char *ptr_byte1 = &byte1;
    char *ptr_byte2 = &byte2;
    int *ptr_a1 = &a1;
    int *ptr_a2 = &a2;
    float *ptr_f1 = &f1;
    float *ptr_f2 = &f2;

    cout<<ptr_byte1<<" "<<byte1<<endl;
    cout<<ptr_byte2<<" "<<byte2<<endl;
    cout<<ptr_a1<<" "<<a1<<endl;
    cout<<ptr_a2<<" "<<a2<<endl;
    cout<<ptr_f1<<" "<<a1<<endl;
    cout<<ptr_f2<<" "<<a2<<endl;
    
    int move_b12 = ptr_byte2 - ptr_byte1;
    int move_a12 = ptr_a2 - ptr_a1;
    int move_f12 = ptr_f2 - ptr_f1;
    
    cout<<move_b12<<" ";
    cout<<move_a12<<" ";
    cout<<move_f12<<endl;
   
    char *sum_b = ptr_byte1 + 3;
    int *sum_a = ptr_a1 + 2;
    float *sum_f = ptr_f1 + 1;
    
    cout<<sum_b<<" ";
    cout<<sum_a<<" ";
    cout<<sum_f<<endl;
    
    int *ptr_temp = ptr_a1;
    ptr_a1 = ptr_a2;
    ptr_a2 = ptr_temp;
    
    cout<<ptr_a2<<" "<<a2<<endl;
    cout<<ptr_f1<<" "<<a1<<endl;
}
