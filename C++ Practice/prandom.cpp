#include <random>
#include <ctime>

void prandom_init(){
    srand( time(0) );
}

int prandom(int max){
    return rand() % max;
}
int prandom(int min , int max){
    return rand() % (max-min) + min;
}

void prandom_string(char* str, int len){
    for (int i=0; i<len-1; i++) {
        str[i] = prandom(97, 123);
    }
    str[len-1]=0;
}
