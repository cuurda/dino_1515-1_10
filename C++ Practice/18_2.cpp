#include <iostream>
using namespace std;

enum distance {
   dm = 1, km, meter, mm, cm
};
int main(){
    int unit;
    float num;
    cout << "Введите номер единицы и значение раделенные побелами: ";
    cin >> unit >> num;
    switch (unit) {
        case dm:
            cout<<num/10<<" m\n";
        break;
        case km:
            cout<<num*1000<<" m\n";
        break;
        case meter:
            cout<<num<<" m\n";
        break;
        case mm:
            cout<<num/1000<<" m\n";
        break;
        case cm:
            cout<<num/100<<" m\n";
        break;
        default:
            cout << "Некорректная единица измерения\n";
        break;
    }
    return 0;
}
