#include <iostream>
using namespace std;

enum number {
   one = 1, two, three, four, five
};
int main(){
    int num;
    cout << "Введите количество разрядов: ";
    cin >> num;
    switch (num) {
        case one:
            cout<<"min: 0 | max: 9\n";
        break;
        case two:
            cout<<"min: 10 | max: 99\n";
        break;
        case three:
            cout<<"min: 100 | max: 999\n";
        break;
        case four:
            cout<<"min: 1000 | max: 9999\n";
        break;
        case five:
            cout<<"min: 10000 | max: 99999\n";
        break;
        default:
            cout << "Некорректная длинна числа\n";
        break;
    }
    return 0;
}
