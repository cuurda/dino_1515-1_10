//
//  str.cpp
//  
//
//  Created by Nikita Levonovich on 16.09.16.
//
//
#include <iostream>
#include <cstdio>

int main(){
    char str[256];
    int place = 0;
    gets(str);
    for (int i=0; i< 256; i++){
        if (str[i] == '\0'){
            place = i;
            break;
        }
    }
    char str2[place-1];
    char* p, *p1, *p2;
    p = &str[0];
    p1 = p + 1;
    char b = *p1;
    p2 = p + place - 2;
    char c = *p2;
    std::cout<<place<<"\n";
    std::cout<<b<<"\n";
    std::cout<<c<<"\n";
    memcpy(&str2, &b, 4*sizeof(char));
    str2[place-2]='\0';
    for (int i =0; i<place+1;i++){
        printf("%x", str[i]);
        printf("\n");
    }
    for (int i =0; i<place-1;i++){
        printf("%x", str2[i]);
        printf("\n");
    }
    puts(str);
    puts(str2);
    return 0;
}