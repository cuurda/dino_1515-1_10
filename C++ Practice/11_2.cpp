#include <iostream>
#include "prandom.cpp"

#define LEN 4
using namespace std;

int main(){
    int a[LEN][LEN];
    prandom_init();
    for ( int i = 0; i < LEN; i++){
        for ( int j = 0; j<LEN; j++){
            a[i][j] = prandom(0,10);
        }
    }
    int min_ind = 0;
    for ( int i = 0; i < LEN;i++){
        for ( int j = 0; j<LEN; j++){
            cout<<a[i][j]<<" ";
        }
        cout<<endl;
        if (a[min_ind][min_ind] > a[i][i]){
            min_ind = i;
        }
    }
    cout<<endl;
    cout<<a[min_ind][min_ind]<<" ";
    cout<<endl;
}