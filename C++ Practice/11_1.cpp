#include <iostream>
#include <random>
#include <ctime>
#include "minmax.cpp"

#define LEN 14
using namespace std;

int main(){
    int a[LEN];
    srand( time(0) );
    for ( int i = 0; i < LEN; i++){
        a[i] = rand()%10;
    }
    for ( int i = 0; i < LEN;i++){
        cout<<a[i]<<" ";
    }
    cout<<endl;
    int min_i = ret_min(a, LEN);
    int max_i = ret_max(a, LEN);
    int temp = a[max_i];
    a[max_i] = a[min_i];
    a[min_i] = temp;
    for ( int i = 0; i < LEN; i++){
        cout<<a[i]<<" ";
    }
    cout<<endl;
}