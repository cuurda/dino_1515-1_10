#include <iostream>
#include <cmath>
using namespace std;

int main() {
	double x,res;
	cout<<"Введите x: ";
	cin>>x;
	if (x<1.4){
		res = M_PI*x*x-7/(x*x);
	}
	else if(x==1.4){
		res = 4*pow(x,3)+7*sqrt(x);
	}else{
		res = log(x+5*sqrt(abs(x+3)));
	}
	cout<<"F(x)="<< res;
}
