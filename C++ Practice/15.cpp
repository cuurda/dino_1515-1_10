#include <iostream>
#include "prandom.cpp"
using namespace std;

int ret_min(int* a, int len);
int ret_max(int* a, int len);

int main(){
    int arr[14];
    prandom_init();
    
    for (int i = 0; i<14; i++) {
        arr[i]= prandom(100);
    }
    
    for (int i = 0; i<14; i++) {
        cout << *(arr+i)<<" ";
    }
    cout<<endl;

    int min = ret_min(arr, 14);
    int max = ret_max(arr, 14);
    
    int temp = *(arr+min);
    *(arr+min) = *(arr+max);
    *(arr+max) = temp;
    
    for (int i = 0; i<14; i++) {
        cout << *(arr+i)<<" ";
    }
    cout<<endl;
}

int ret_min(int* a, int len){
    int min_ind = 0;
    for ( int i = 1; i < len; i++){
        if (*(a+min_ind) > *(a+i)){
            min_ind = i;
        }
    }
    return min_ind;
}
int ret_max(int* a, int len){
    int max_ind = 0;
    for ( int i = 1; i < len; i++){
        if (*(a+max_ind) < *(a+i)){
            max_ind = i;
        }
        
    }
    return max_ind;
}
