int ret_min(int* a, int len){
    int min_ind = 0;
    for ( int i = 1; i < len; i++){
        if (a[min_ind] > a[i]){
            min_ind = i;
        }
    }
    return min_ind;
}
int ret_max(int* a, int len){
    int max_ind = 0;
    for ( int i = 1; i < len; i++){
        if (a[max_ind] < a[i]){
            max_ind = i;
        }
        
    }
    return max_ind;
}
