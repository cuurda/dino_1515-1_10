#include <iostream>

using namespace std;
int main()
{
	int points = 0, answer=0;
	cout<<"Вас приетствуюет система тестирования.\n";
	cout<<"Тест \"\Комбинаторика\"\n";
	cout<<"Вопрос 1.\n";
	cout<<"Укажите формулу количества k-сочетаний без повторений из n:\n";
	cout<<"\t1. n!/(k!n!)\n";
	cout<<"\t2. k!/(k!n!)\n";
	cout<<"\t3. n!/(k!(n-k)!)\n";
	cout<<"\t4. n!/(n-k)!\n";
	cout<<"Ваш ответ: ";
	cin>>answer; 
	while (!( 0 < answer && answer < 5)){
		cout<<"Некорректный ответ!\n";
		cout<<"Ваш ответ: ";
		cin>>answer;
	}
	points += answer==3 ? 1 : 0;
	cout<<"-------------------------------------------------------\n";
	cout<<"Вопрос 2.\n";
	cout<<"Укажите формулу количества размещений без повторений из n по k:\n";
	cout<<"\t1. n^k\n";
	cout<<"\t2. n!/(k!(n-k)!)\n";
	cout<<"\t3. k!/(n!)\n";
	cout<<"\t4. n!/(n-k)!\n";
	cout<<"Ваш ответ: ";
	cin>>answer;
	while (!( 0 < answer && answer < 5)){
		cout<<"Некорректный ответ!\n";
		cout<<"Ваш ответ: ";
		cin>>answer;
	}
	points += answer==4 ? 1 : 0;
	cout<<"-------------------------------------------------------\n";
	cout<<"Вопрос 3.\n";
	cout<<"Укажите формулу количества размещений c повторениями из n по k:\n";
	cout<<"\t1. n!\n";
	cout<<"\t2. n^k\n";
	cout<<"\t3. k!\n";
	cout<<"\t4. n!/(k!(n-k)!)\n";
	cout<<"Ваш ответ: ";
	cin>>answer;
	while (!( 0 < answer && answer < 5)){
		cout<<"Некорректный ответ!\n";
		cout<<"Ваш ответ: ";
		cin>>answer;
	}
	points += answer==2 ? 1 : 0;
	cout<<"-------------------------------------------------------\n";
	cout<<"Вопрос 4.\n";
	cout<<"Укажите формулу количества k-сочетаний c повторениями из n:\n";
	cout<<"\t1. (n+k-1)!/(k!(n-1)!)\n";
	cout<<"\t2. k!/(k!n!)\n";
	cout<<"\t3. n!/(k!n!)\n";
	cout<<"\t4. n!/((n-k)!)\n";
	cout<<"Ваш ответ: ";
	cin>>answer;
	while (!( 0 < answer && answer < 5)){
		cout<<"Некорректный ответ!\n";
		cout<<"Ваш ответ: ";
		cin>>answer;
	}
	points += answer==1 ? 1 : 0;
	cout<<"-------------------------------------------------------\n";
	cout<<"Вопрос 5.\n";
	cout<<"Укажите формулу количества перемещений n объектов:\n";
	cout<<"\t1. (n+k-1)!/(k!(n-1)!)\n";
	cout<<"\t2. k!/(k!n!)\n";
	cout<<"\t3. n!/(k!n!)\n";
	cout<<"\t4. n!\n";
	cout<<"Ваш ответ: ";
	cin>>answer;
	while (!( 0 < answer && answer < 5)){
		cout<<"Некорректный ответ!\n";
		cout<<"Ваш ответ: ";
		cin>>answer;
	}
	points += answer==4 ? 1 : 0;
	cout<<"-------------------------------------------------------\n";
	cout << "Результат: "<< points<< " баллов"<<endl; 
	switch (points)
	{
		case 0:
		case 1:
		case 2: cout << "Bad."      ; break;
		case 3: cout << "Normal."   ; break;
		case 4: cout << "Well."     ; break;
		case 5: cout << "Very Well."; break;
	}
	return 0;
}
