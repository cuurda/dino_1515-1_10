#include <iostream>
#include <string>

using namespace std;

int main() {
	int num, pre=10;
	bool is_inc=true;
	string answer=""; 
	
	
	cin>>num;	
	while(num){
		int temp = num % 10;
		num/=10;
		if (pre<temp){
			is_inc = false;
			break;
		}
		pre = temp;
	}
	answer = is_inc ? "Да" : "Нет";
	cout<< answer;
}
