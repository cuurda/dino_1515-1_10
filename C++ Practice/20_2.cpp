#include <iostream>
using namespace std;

int degree5 (int n);

int main(){
    int n;
    cout<<"Введите число: ";
    cin>>n;
    cout<<degree5(n)<<endl;
}

int degree5 (int n){
    if ( n == 1){
        return 0;
    }
    if (n % 5 == 0) {
        int d = degree5(n/5);
        return d == -1 ? -1 : 1 + d;
    }else{
        return -1;
    }
    
}
