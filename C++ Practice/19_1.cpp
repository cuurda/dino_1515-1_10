#include <iostream>
using namespace std;

int mypow(int a, int n);

int main(){
    cout<<mypow(5,3)+mypow(3,4)+mypow(2,7)<<endl;
    return 0;
}

int mypow(int a, int n){
    int res = a;
    for (int i = 0; i<n - 1; i++) {
        res*=a;
    }
    return res;
}
