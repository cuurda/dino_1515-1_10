from RuleBox import RuleBox
class MarkovMachine:
    def __init__(self):
        self.rule_box = RuleBox()
        print("Введите строку:")
        self.string = input()
        self.working = True
        print("Входящее слово: ", self.string)
        while self.working:
            self.working = self.step()
        print("Конечное слово: ", self.string)

    def step(self):
        for i in range(self.rule_box.num):
            position = self.string.find(self.rule_box.rules[i][0])
            if position > -1:
                print("Rule: ", self.rule_box.rules[i][0],
                      ["","|"][self.rule_box.terminmator.count(i) > 0],
                      "=>", self.rule_box.rules[i][1])
                pre_string = self.string[:position]
                change = self.rule_box.rules[i][1]
                post_string = self.string[position + len(self.rule_box.rules[i][0]):]
                self.string = pre_string + change + post_string
                print(self.string)
                for j in range(len(self.rule_box.terminmator)):
                    if i == self.rule_box.terminmator[j]:
                        return False
                return True
        return False

MarkovMachine()