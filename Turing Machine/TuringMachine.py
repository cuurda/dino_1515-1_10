from Alphabet import Alphabet
from Proccesser import Processer
from RuleBox import RuleBox
from Line import Line

class TuringMachine:
    def __init__(self):
        self.processer = Processer()
        self.alphabet = Alphabet()
        self.rule_box = RuleBox(self.alphabet, self.processer)
        self.line = Line()
        self.mode = "+"
        word = input()
        print(word)
        self.line.write_word_in_begin(word)
        self.pointer = [0, len(word) - 1][len(word) - 1 > 0]
        self.state = self.processer.states[0]
        self.line.print_line(self.pointer)
        print("Current state: ", self.state)
        while self.state != self.processer.states[len(self.processer.states) - 1]:
            self.step()
            self.line.print_line(self.pointer)
            print("Current state: ", self.state)

    def step(self):
        letter = self.line.read(self.pointer)
        rule = self.rule_box.rule_list[(self.state, letter)]
        print("Rule: ",rule)
        self.state = rule[0]
        self.line.write(self.pointer, rule[1])
        self.move(rule[2])

    def move(self, direction : str) -> None:
        if direction == "R":
            self.pointer += 1
            if self.pointer >= len(self.line.line):
                if self.mode == "+":
                    self.line.line.append(None)
                if self.mode == "-":
                    self.pointer = 0
                    self.mode = "+"
        elif direction == "L":
                self.pointer -= 1
                if self.pointer < 0:
                    if self.mode == "+":
                        self.line.line.append(None)
                        self.pointer = len(self.line.line) - 1
                        self.mode = "-"
                if self.mode == "-":
                    self.line.line.append(None)
                    self.pointer += 1
        elif direction != "N":
            print("Error")

turing_machine = TuringMachine()