class Line:
    def __init__(self) -> None:
        self.line = [None for a in range(20)]
        self.print_line(0)

    def print_line(self, pointer : int) -> None:
        string = ""
        string2 = ""
        lenght = -1
        for i in range(len(self.line)):
            string += "|" + str(self.line[i])
            delta = len(string) - lenght
            patern = '{:^' + str(delta) + '}'
            string2 += patern.format(['_','^'][i == pointer])
            lenght = len(string)
        string += "||\n"
        string += string2
        print(string)

    def write_word_in_begin(self, word : str) -> None:
        for i in range(len(word)):
            self.line[i] = word[i]

    def read(self, pointer : int) -> object:
        return self.line[pointer]

    def write(self, pointer : int, letter : object) -> None:
        self.line[pointer] = letter
