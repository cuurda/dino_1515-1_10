from Alphabet import Alphabet
from Proccesser import Processer

class RuleBox:
    def __init__(self, alphabet : Alphabet, processer : Processer ) -> None:
        self.rule_list = dict()
        self.num_rules = 0
        for i in range(len(processer.states)-1):
            for j in range(len(alphabet.alphabet)):
                new_state = input().split()
                new_state[1] = [new_state[1], None][new_state[1] == "None"]
                print(new_state)
                self.rule_list[(processer.states[i], alphabet.alphabet[j])] = (new_state[0], new_state[1], new_state[2])
        print(self.rule_list)

